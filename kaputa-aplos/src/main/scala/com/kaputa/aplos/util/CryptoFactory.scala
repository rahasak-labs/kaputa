package com.kaputa.aplos.util

import com.kaputa.aplos.config.AppConf
import org.bouncycastle.asn1.{ASN1Integer, ASN1Sequence}
import org.bouncycastle.jcajce.provider.digest.Keccak
import org.bouncycastle.util.Arrays
import org.bouncycastle.util.encoders.Hex
import org.web3j.crypto.{ECKeyPair, Keys, Sign}
import org.web3j.utils.Numeric

import java.io.{File, FileInputStream, FileOutputStream}
import java.math.BigInteger
import java.security._
import java.security.spec.{ECGenParameterSpec, PKCS8EncodedKeySpec, RSAPublicKeySpec, X509EncodedKeySpec}
import java.util.Base64
import javax.crypto.Cipher
import scala.util.{Failure, Success, Try}

object CryptoFactory extends AppConf with AppLogger {

  def init(): Unit = {
    CryptoFactory.initRSAKeys()
    logger.info(s"init Crypto factory, pubkey - ${CryptoFactory.loadRSAPublicKey()}")
  }

  def initRSAKeys(): Unit = {
    // first create .keys directory
    val dir: File = new File(keysDir)
    if (!dir.exists) {
      dir.mkdir
    }

    // generate keys if not exists
    val filePublicKey = new File(publicKeyLocation)
    if (!filePublicKey.exists) {
      generateRSAKeyPair()
    }
  }

  def generateRSAKeyPair(): Unit = {
    // generate key pair
    val keyPairGenerator = KeyPairGenerator.getInstance("RSA")
    keyPairGenerator.initialize(keysSize, new SecureRandom)
    val keyPair: KeyPair = keyPairGenerator.generateKeyPair

    // save public key
    val x509keySpec = new X509EncodedKeySpec(keyPair.getPublic.getEncoded)
    val publicKeyStream = new FileOutputStream(publicKeyLocation)
    publicKeyStream.write(x509keySpec.getEncoded)

    // save private key
    val pkcs8KeySpec = new PKCS8EncodedKeySpec(keyPair.getPrivate.getEncoded)
    val privateKeyStream = new FileOutputStream(privateKeyLocation)
    privateKeyStream.write(pkcs8KeySpec.getEncoded)
  }

  def loadRSAKeyPair(): KeyPair = {
    // read public key
    val filePublicKey = new File(publicKeyLocation)
    var inputStream = new FileInputStream(publicKeyLocation)
    val encodedPublicKey: Array[Byte] = new Array[Byte](filePublicKey.length.toInt)
    inputStream.read(encodedPublicKey)
    inputStream.close()

    // read private key
    val filePrivateKey = new File(privateKeyLocation)
    inputStream = new FileInputStream(privateKeyLocation)
    val encodedPrivateKey: Array[Byte] = new Array[Byte](filePrivateKey.length.toInt)
    inputStream.read(encodedPrivateKey)
    inputStream.close()

    val keyFactory: KeyFactory = KeyFactory.getInstance("RSA")

    // public key
    val publicKeySpec: X509EncodedKeySpec = new X509EncodedKeySpec(encodedPublicKey)
    val publicKey: PublicKey = keyFactory.generatePublic(publicKeySpec)

    // private key
    val privateKeySpec: PKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey)
    val privateKey: PrivateKey = keyFactory.generatePrivate(privateKeySpec)

    new KeyPair(publicKey, privateKey)
  }

  def loadRSAPublicKey(): String = {
    // get public key via key pair
    val keyPair = loadRSAKeyPair()
    val publicKeyStream = keyPair.getPublic.getEncoded

    // BASE64 encoded string
    Base64.getEncoder.encodeToString(publicKeyStream).replaceAll("\n", "").replaceAll("\r", "")
  }

  def getRSAPublicKey(pubkey: String): Option[PublicKey] = {
    def getAppleKey: Try[PublicKey] = {
      Try {
        // these will throw exception in case of type mismatch
        val sequence = ASN1Sequence.getInstance(Base64.getDecoder.decode(pubkey))
        val modulus = ASN1Integer.getInstance(sequence.getObjectAt(0))
        val exponent = ASN1Integer.getInstance(sequence.getObjectAt(1))
        val keySpec = new RSAPublicKeySpec(modulus.getPositiveValue, exponent.getPositiveValue)
        val factory = KeyFactory.getInstance("RSA")
        factory.generatePublic(keySpec)
      }
    }

    def getAndroidKey: Try[PublicKey] = {
      Try {
        val spec = new X509EncodedKeySpec(Base64.getDecoder.decode(pubkey))
        val kf = KeyFactory.getInstance("RSA")
        kf.generatePublic(spec)
      }
    }

    getAndroidKey match {
      case Success(pk) => Option(pk)
      case Failure(_) => getAppleKey.toOption
    }
  }

  def signWithRSA(payload: String): String = {
    // get private key via key pair
    val keyPair = loadRSAKeyPair()
    val privateKey = keyPair.getPrivate

    // sign the payload
    val signature: Signature = Signature.getInstance("SHA256withRSA")
    signature.initSign(privateKey)
    signature.update(payload.getBytes)

    // signature as Base64 encoded string
    Base64.getEncoder.encodeToString(signature.sign).replaceAll("\n", "").replaceAll("\r", "")
  }

  def verifyRSASignature(payload: String, signedPayload: String): Boolean = {
    // get public key via key pair
    val keyPair = loadRSAKeyPair()
    val publicKey = keyPair.getPublic

    val signature = Signature.getInstance("SHA256withRSA")
    signature.initVerify(publicKey)
    signature.update(payload.getBytes)

    // decode(BASE64) signed payload and verify signature
    signature.verify(Base64.getDecoder.decode(signedPayload))
  }

  def verifySignature1(payload: String, signedPayload: String, publicKey: Option[PublicKey]): Boolean = {
    publicKey.exists { pk =>
      val signature = Signature.getInstance("SHA256withRSA")
      signature.initVerify(pk)
      signature.update(payload.getBytes)

      // decode(BASE64) signed payload and verify signature
      signature.verify(Base64.getDecoder.decode(signedPayload))
    }
  }


  def encrypt(payload: String, publicKey: PublicKey): Array[Byte] = {
    val cipher: Cipher = Cipher.getInstance("RSA")
    cipher.init(Cipher.ENCRYPT_MODE, publicKey)

    cipher.doFinal(payload.getBytes)
  }

  def decrypt(payload: Array[Byte], privateKey: PrivateKey): String = {
    val cipher: Cipher = Cipher.getInstance("RSA")
    cipher.init(Cipher.DECRYPT_MODE, privateKey)
    new String(cipher.doFinal(payload))
  }

  def sha256(payload: String): String = {
    val digest = MessageDigest.getInstance("SHA-256")
    val hash = digest.digest(payload.getBytes)

    Base64.getEncoder.encodeToString(hash).replaceAll("\n", "").replaceAll("\r", "")
  }

  def encode(paylod: String): String = {
    // signature as Base64 encoded string
    Base64.getEncoder.encodeToString(paylod.getBytes())
      .replaceAll("\n", "")
      .replaceAll("\r", "")
  }

  def dehydrate(msg: String, remove: String): String = {
    msg.replace(remove, "")
      .replace("digsig", "")
      .replaceAll("\"", "")
      .replaceAll("[\\[\\](){}]", "")
      .replaceAll(" ", "")
      .replaceAll(",", "")
      .replaceAll(":", "")
      .replaceAll("\n", "")
      .replaceAll("\r", "")
      .toLowerCase()
      .sortWith(_ < _)
  }

  //  ----------- ECDSA ------------
  //   Private key is 64 character long
  //   public key is 128 character long
  //   address is 40 character long excluding the 0x prefix
  //   signature is 130 character long exluding the 0x prefix

  //load ECDSA key pair
  def loadECDSAKeyPair(): KeyPair = {
    // read public key
    val filePublicKey = new File(ecdsaPublicKeyLocation)
    var inputStream = new FileInputStream(ecdsaPublicKeyLocation)
    val encodedPublicKey: Array[Byte] = new Array[Byte](filePublicKey.length.toInt)
    inputStream.read(encodedPublicKey)
    inputStream.close()

    // read private key
    val filePrivateKey = new File(ecdsaPrivateKeyLocation)
    inputStream = new FileInputStream(ecdsaPrivateKeyLocation)
    val encodedPrivateKey: Array[Byte] = new Array[Byte](filePrivateKey.length.toInt)
    inputStream.read(encodedPrivateKey)
    inputStream.close()

    val keyFactory: KeyFactory = KeyFactory.getInstance("EC")

    // public key
    val publicKeySpec: X509EncodedKeySpec = new X509EncodedKeySpec(encodedPublicKey)
    val publicKey: PublicKey = keyFactory.generatePublic(publicKeySpec)

    // private key
    val privateKeySpec: PKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey)
    val privateKey: PrivateKey = keyFactory.generatePrivate(privateKeySpec)

    new KeyPair(publicKey, privateKey)

  }

  //init ECDSA keys
  def initECDSAKeys(): Unit = {
    // first create .keys directory
    val dir: File = new File(keysDir)
    if (!dir.exists) {
      dir.mkdir
    }

    // generate keys if not exists
    val filePublicKey = new File(ecdsaPublicKeyLocation)
    if (!filePublicKey.exists) {
      generateECDSAKeyPair()
    }
  }

  //generate ECDSA key pair
  def generateECDSAKeyPair(): Unit = {
    // generate key pair
    val keyPairGenerator = KeyPairGenerator.getInstance("EC")
    keyPairGenerator.initialize(new ECGenParameterSpec("secp256k1"), new SecureRandom)
    val keyPair: KeyPair = keyPairGenerator.generateKeyPair


    // save public key
    val x509keySpec = new X509EncodedKeySpec(keyPair.getPublic.getEncoded)
    println(s"x509keySpec.getEncoded -${x509keySpec.getEncoded}")
    val publicKeyStream = new FileOutputStream(ecdsaPublicKeyLocation)
    publicKeyStream.write(x509keySpec.getEncoded)

    // save private key
    val pkcs8KeySpec = new PKCS8EncodedKeySpec(keyPair.getPrivate.getEncoded)
    println(s"pkcs8KeySpec.getEncoded -${pkcs8KeySpec.getEncoded}")
    val privateKeyStream = new FileOutputStream(ecdsaPrivateKeyLocation)
    privateKeyStream.write(pkcs8KeySpec.getEncoded)
  }

  //verify signature with ECDSA
  def verifySignatureECDSA(payload: String, signedPayload: String, publicKey: Option[PublicKey]): Boolean = {
    val signature = Signature.getInstance("SHA256withECDSA")

    val pk = publicKey.getOrElse(loadECDSAKeyPair().getPublic)

    signature.initVerify(pk)
    signature.update(payload.getBytes)

    // decode(BASE64) signed payload and verify signature
    signature.verify(Base64.getDecoder.decode(signedPayload))
  }

  def compressECDSAPubKey(pubKey: BigInteger): String = {
    val pubKeyYPrefix = if (pubKey.testBit(0)) "03"
    else "02"
    val pubKeyHex = pubKey.toString(16)
    val pubKeyX = pubKeyHex.substring(0, 64)
    pubKeyYPrefix + pubKeyX
  }

  /** create ECDSA keypair from private key
   *
   * @param pk private key
   * @return ECKeyPair
   */
  def getECDSAKeypair(pk: String): ECKeyPair = {
    val privKey = new BigInteger(pk, 16)
    ECKeyPair.create(privKey)
  }

  /**
   * get ECDSA public key from private key
   *
   * @param pk private key
   * @return String (public key)
   */
  def getECDSAPublicKey(pk: String): String = {
    val privateKey = new BigInteger(pk, 16)
    val keyPair = ECKeyPair.create(privateKey)
    keyPair.getPublicKey.toString(16)
  }

  /**
   * get ECDSA public address from private key
   *
   * @param pk       private key
   * @param checksum true if checksum is required
   * @return String (public address)
   */
  def getECDSAAddress(pk: String, checksum: Boolean = false): String = {
    val privateKey = new BigInteger(pk, 16)
    val keyPair = ECKeyPair.create(privateKey)
    val add = Keys.getAddress(keyPair)
    if (checksum) Keys.toChecksumAddress(add) else "0x" + add
  }

  /**
   * sign message with ECDSA
   *
   * @param payload message to sign
   * @param keyPair ECKeyPair
   * @return String (signature)
   */
  def signWithECDSA(payload: String, keyPair: ECKeyPair): String = {
    // sign the payload
    //keccak256 hash of the payload
    val digest256 = new Keccak.Digest256()
    val msgHash = digest256.digest(payload.getBytes("UTF-8"))

    val signature = Sign.signMessage(msgHash, keyPair, false)

    //byte[] to BigInteger
    val r = new BigInteger(1, signature.getR)
    val s = new BigInteger(1, signature.getS)
    val v = new BigInteger(1, signature.getV)

    println("Msg: " + payload)
    println("Msg hash: " + Hex.toHexString(msgHash))
    println(s"Signature v: ${Hex.toHexString(signature.getV)}, r = ${Hex.toHexString(signature.getR)}, s = ${Hex.toHexString(signature.getS)}\n")
    println(s"Signature v: $v, r = $r, s = $s\n")

    //encode signature
    //        return s"0x$r$s$v"
    s"0x${Hex.toHexString(signature.getR)}${Hex.toHexString(signature.getS)}${Hex.toHexString(signature.getV)}"
  }

  /**
   * verify signature with ECDSA ( no need to hash the payload unless you have double hashed to sign the payload )
   *
   * @param payload   message to verify
   * @param signature signature to verify
   * @param publicKey public key associated with signature (BigInteger)
   * @return true if signature is valid false otherwise
   */
  def verifyECDSASignature(payload: String, signature: String, publicKey: String): Boolean = {
    val signature1 = if (signature.startsWith("0x")) signature.substring(2) else signature
    println(s"signature - $signature1")
    val signatureBytes = Numeric.hexStringToByteArray(signature1)
    //    println(s"signatureBytes - ${signatureBytes.toList} : ${signatureBytes.length}")
    var v = signatureBytes(64)
    // if v is less than 27, then add 27 to it
    //usually v is 27 or 28
    if (v < 27) {
      v = (v + 27).toByte
    }
    //    val r = new BigInteger(1, signatureBytes.slice(0, 32))
    //    val s = new BigInteger(1, signatureBytes.slice(32, 64))
    //    val v1 = new BigInteger(1, Array(v))
    //    println(s"v: $v, r = $r, s = $s\n")
    val signatureData = new Sign.SignatureData(v, Arrays.copyOfRange(signatureBytes, 0, 32), Arrays.copyOfRange(signatureBytes, 32, 64))

    val digest256 = new Keccak.Digest256()
    val msgHash = digest256.digest(payload.getBytes("UTF-8"))

    val pubKeyRecovered = Sign.signedMessageHashToKey(msgHash, signatureData).toString(16)
    println("Recovered public key: " + pubKeyRecovered)

    val validSig = publicKey.equals(pubKeyRecovered)
    println("Signature valid? " + validSig)
    validSig
  }

  /**
   * get address from public key
   *
   * @param publicKey public key
   *                  (hex string)
   * @return String (address)
   */
  def getAddressFromPublicKey(publicKey: String): String = {
    val pubKey = Keys.getAddress(publicKey)
    pubKey

  }
}

//object ECDSA1Test extends App{

//  val pk = "6c5c62c85c4689f6786ff5170e41d1c3399afb51e9de85da043c98b788f6a144"
//  val privKey = new BigInteger(pk,16)
//  var EC =ECKeyPair.create(privKey)
//
//  val pubKey = EC.getPublicKey
//  println("Private key: " + pk)
//  println("Public key: " + pubKey.toString(16))
//  println(s"add :${Keys.getAddress(EC)}")
//  println(s"add :${Keys.toChecksumAddress(Keys.getAddress(EC))}")
//
//
//  val msg = "Hallo world"
//  val msgHash = Hash.sha3(msg.getBytes)
//  val signature = Sign.signMessage(msgHash, EC, false)
//  println("Msg: " + msg)
//  println("Msg hash: " + Hex.toHexString(msgHash))
//  println(s"Signature: ${Hex.toHexString(signature.getV)}, r = ${Hex.toHexString(signature.getR)}, s = ${Hex.toHexString(signature.getS)}\n")
//
//  //byte[] to BigInteger
//  val r = new BigInteger(1, signature.getR)
//  val s = new BigInteger(1, signature.getS)
//  val v = new BigInteger(1, signature.getV)
//  println(s"Signature: r = ${r}, s = ${s}, v = ${v}")
//
//  //BIgInteger to toHexString
//  println(s"Signature: r = ${r.toString(16)}, s = ${s.toString(16)}, v = ${v.toString(16)}")
//  println()
////
//  val pubKeyRecovered = Sign.signedMessageToKey(msg.getBytes, signature)
//  println("Recovered public key: " + pubKeyRecovered)
////
//  val validSig = pubKey.equals(pubKeyRecovered)
//  println("Signature valid? " + validSig)
//
//}

//object ECDSATest extends App {
//
//  val pk = "6c5c62c85c4689f6786ff5170e41d1c3399afb51e9de85da043c98b788f6a144"
//  val pubadd = "0xbc4d06088777a6a6df24d9fa7b06095d3b73dec9"
//  val pub = "532cf36a18cce3cee3c484ad0cdffcc16e3b0097d381c4add1ccf1526425f0e3b2f3148d2cf048b1737b1ddf99d6e10405b6c4d419b4f2236f960948a695a6be"
//  val pubaddckhsum = "0xbC4d06088777a6A6DF24d9Fa7B06095d3b73DEC9"
//
//  val privKey = new BigInteger(pk, 16)
//  val EC = getECDSAKeypair(pk);
//  val publicKey = getECDSAPublicKey(pk);
//  val address = getECDSAAddress(pk);
//  val addressChecksum = getECDSAAddress(pk, true);
//
//  //DateFactory.timestamp().toString
//  var payload = "Hallo world"  ;
//
////  import com.kaputa.aplos.protocol.TokenMessageProtocol.createFormat
//
////  val j = msg.toJson.asJsObject
////  val payload = j.fields.values.toSeq.map(_.toString.replaceAll("\"", "")).sortWith(_ < _).mkString
//
//  println(s"privKey : $privKey")
//  println(s"pubKey : $publicKey")
//  println(s"address : $address")
//  println(s"addressChecksum : $addressChecksum")
////  println(s"msg : $j")
//  println(s"payload : $payload")
//
//  val sign = signWithECDSA(payload, EC)
//  println(s"sign : $sign")
//  val msgHash = Hash.sha3(payload.getBytes)
//  val verify = verifyECDSASignature(payload, sign, publicKey)
//
//  println(s"verify : $verify")
//  //--------OUTPUT------------
//  //  privKey : 49013019349801449068912387704179356298451674907571404307115490394731875574084
//  //  pubKey : 532cf36a18cce3cee3c484ad0cdffcc16e3b0097d381c4add1ccf1526425f0e3b2f3148d2cf048b1737b1ddf99d6e10405b6c4d419b4f2236f960948a695a6be
//  //  address : 0xbc4d06088777a6a6df24d9fa7b06095d3b73dec9
//  //  addressChecksum : 0xbC4d06088777a6A6DF24d9Fa7B06095d3b73DEC9
//  //  msg : {"decimals":"8","execer":"0xbc4d06088777a6a6df24d9fa7b06095d3b73dec9","id":"hh","messageType":"create","name":"Nilaan","symbol":"Nil","totalSupply":"450"}
//  //  payload : 0xbc4d06088777a6a6df24d9fa7b06095d3b73dec94508NilNilaancreatehh
//  //  Msg: 0xbc4d06088777a6a6df24d9fa7b06095d3b73dec94508NilNilaancreatehh
//  //  Msg hash: 3e6fbcc19902b3ae4fb00247cb0120fad8c83fda890db38c084011524e545b13
//  //  Signature: 1b, r = 1350e7ae1c17b0330ec13c6fd76fc058f9e36b0485013be0b7a698033b228cd3, s = 7debe9dc818aa6621a67c677e2e5abe14894c362cfa40a9bebef470cdf3d6f70
//  //
//  //  Signature: 27, r = 8736890885659649789420008134368520654747656304837234388997474575191124380883, s = 56955929184860610738611055654927010432059393562067173978344553067433063968624
//  //
//  //  sign : 0x1350e7ae1c17b0330ec13c6fd76fc058f9e36b0485013be0b7a698033b228cd37debe9dc818aa6621a67c677e2e5abe14894c362cfa40a9bebef470cdf3d6f701b
//  //  signature - 1350e7ae1c17b0330ec13c6fd76fc058f9e36b0485013be0b7a698033b228cd37debe9dc818aa6621a67c677e2e5abe14894c362cfa40a9bebef470cdf3d6f701b
//  //  Recovered public key: 532cf36a18cce3cee3c484ad0cdffcc16e3b0097d381c4add1ccf1526425f0e3b2f3148d2cf048b1737b1ddf99d6e10405b6c4d419b4f2236f960948a695a6be
//  //  Signature valid? true
//  //  verify : true
//  //
//  //  Process finished with exit code 0
//
//
//}