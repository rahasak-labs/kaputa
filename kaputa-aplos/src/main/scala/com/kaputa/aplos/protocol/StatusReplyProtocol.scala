package com.kaputa.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

case class StatusReply(code: Int, msg: String)

object StatusReplyProtocol extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val statusFormat = jsonFormat2(StatusReply)
}
