package com.kaputa.aplos.protocol

import com.kaputa.aplos.actor.AccountActor.{CreateAccount, GetAccount}
import com.kaputa.aplos.actor.AccountActor.{CreateAccount, GetAccount}
import spray.json._

trait AccountMessage

object AccountMessageProtocol extends DefaultJsonProtocol {

  implicit val createAccountFormat: JsonFormat[CreateAccount] = jsonFormat6(CreateAccount)
  implicit val getAccountFormat: JsonFormat[GetAccount] = jsonFormat4(GetAccount)

  implicit object AccountMessageFormat extends RootJsonFormat[AccountMessage] {
    def write(obj: AccountMessage): JsValue =
      JsObject((obj match {
        case m: CreateAccount => m.toJson
        case m: GetAccount => m.toJson
        case unknown => deserializationError(s"json deserialize error: $unknown")
      }).asJsObject.fields)

    def read(json: JsValue): AccountMessage =
      json.asJsObject.getFields("messageType") match {
        case Seq(JsString("create")) => json.convertTo[CreateAccount]
        case Seq(JsString("get")) => json.convertTo[GetAccount]
        case unrecognized => serializationError(s"json serialization error $unrecognized")
      }
  }

}
