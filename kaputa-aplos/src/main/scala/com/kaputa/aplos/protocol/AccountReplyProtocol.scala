package com.kaputa.aplos.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class AccountReply(address: String,
                        name: String,
                        pubkey: String)

object AccountReplyProtocol extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val accountReplyFormat = jsonFormat3(AccountReply)
}
