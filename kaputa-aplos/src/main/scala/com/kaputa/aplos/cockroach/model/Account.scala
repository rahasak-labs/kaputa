package com.kaputa.aplos.cockroach.model

import com.kaputa.aplos.config.CockroachConf
import slick.jdbc.PostgresProfile.api._
import slick.lifted.Tag

case class Account(address: String, name: String, pubkey: String)

trait AccountsTable {
  this: CockroachConf =>

  class Accounts(tag: Tag) extends Table[Account](tag, "accounts") {
    def address = column[String]("address", O.PrimaryKey, O.Unique)

    def name = column[String]("name")

    def pubkey = column[String]("pubkey")

    // select
    def * = (address, name, pubkey) <> (Account.tupled, Account.unapply)
  }

  val accounts = TableQuery[Accounts]
}

