package com.kaputa.aplos.cockroach

import com.kaputa.aplos.cockroach.model.{Account, AccountsTable}
import com.kaputa.aplos.config.CockroachConf
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object CockroachRepo extends AccountsTable with CockroachConf {

  val waitForDb = 30.seconds

  def init(): List[Unit] = {
    val f4 = db.run(accounts.schema.createIfNotExists)
    Await.result(Future.sequence(List(f4)), waitForDb)
  }

  def createAccount(account: Account): Unit = {
    val t1 = accounts += account

    val combinedAction = DBIO.seq(t1)
    val f = db.run(combinedAction.transactionally)
    Await.result(f, waitForDb)
  }

  def getAccount(address: String): Option[Account] = {
    val f = db.run(accounts.filter(_.address === address).result.headOption)

    Await.result(f, waitForDb)
  }
}
