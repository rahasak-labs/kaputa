package com.kaputa.aplos.actor

import akka.actor.{Actor, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.Directives.{_enhanceRouteWithConcatenation, as, complete, entity, get, onComplete, pathPrefix, post, withoutSizeLimit}
import akka.http.scaladsl.server.{Route, StandardRoute}
import akka.pattern.ask
import akka.stream.{Materializer, Supervision, SystemMaterializer}
import akka.util.Timeout
import com.kaputa.aplos.actor.AccountActor.{CreateAccount, GetAccount}
import com.kaputa.aplos.actor.HttpActor.Serve
import com.kaputa.aplos.config.AppConf
import com.kaputa.aplos.protocol.{AccountMessage, AccountReply, StatusReply}
import com.kaputa.aplos.util.AppLogger

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}

object HttpActor {
  case class Serve()

  def props()(implicit system: ActorSystem): Props = Props(new HttpActor)
}

class HttpActor()(implicit system: ActorSystem) extends Actor with AppLogger with AppConf {

  override def receive: Receive = {
    case Serve =>
      val decider: Supervision.Decider = { e =>
        logError(e)
        Supervision.Resume
      }
      implicit val materializer: Materializer = SystemMaterializer(system.classicSystem).materializer

      implicit val ec: ExecutionContextExecutor = system.dispatcher

      Http().bindAndHandle(route, "0.0.0.0", servicePort)
  }

  def route()(implicit materializer: Materializer, ec: ExecutionContextExecutor): Route = withoutSizeLimit {
    implicit val timeout: Timeout = Timeout(60.seconds)

    import com.kaputa.aplos.protocol.StatusReplyProtocol._

    pathPrefix("api") {
      pathPrefix("v1") {
        pathPrefix("account") {
          get {
            complete(HttpEntity(ContentTypes.`application/json`, "account contract"))
          } ~ post {
            import com.kaputa.aplos.protocol.AccountMessageProtocol._

            entity(as[AccountMessage]) {
              case create: CreateAccount =>
                val f = context.actorOf(AccountActor.props) ? create
                onComplete(f) {
                  any: Any => handleResponse(any)
                }
              case get: GetAccount =>
                val f = context.actorOf(AccountActor.props) ? get
                onComplete(f) {
                  any: Any => handleResponse(any)
                }
            }
          }
        }
      }
    }
  }

  def handleResponse(msg: Any): StandardRoute = {
    msg match {
      case Success(value: AccountReply) =>
        import com.kaputa.aplos.protocol.AccountReplyProtocol._
        complete(StatusCodes.OK -> value)
      case Success(value: StatusReply) =>
        import com.kaputa.aplos.protocol.StatusReplyProtocol._
        complete(StatusCodes.OK -> value)
      case Failure(exception) =>
        complete(StatusCodes.BadRequest -> exception.getMessage)
      case _ =>
        complete(StatusCodes.BadRequest -> "Unknown error")
    }
  }
}

