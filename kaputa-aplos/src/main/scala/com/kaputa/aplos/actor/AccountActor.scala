package com.kaputa.aplos.actor

import akka.actor.{Actor, Props}
import akka.stream.Materializer
import com.kaputa.aplos.actor.AccountActor.{CreateAccount, GetAccount}
import com.kaputa.aplos.cockroach.CockroachRepo.{createAccount, getAccount}
import com.kaputa.aplos.cockroach.model.Account
import com.kaputa.aplos.config.AppConf
import com.kaputa.aplos.protocol.{AccountMessage, AccountReply, StatusReply}
import com.kaputa.aplos.util.AppLogger


object AccountActor {
  val ACCOUNT_ACTOR = "AccountActor"

  case class AuthAccountMessage(authToken: Option[String], accountMessage: AccountMessage)

  case class CreateAccount(messageType: String, execer: String, id: String, address: String, name: String, pubkey: String) extends AccountMessage

  case class GetAccount(messageType: String, execer: String, id: String, address: String) extends AccountMessage

  def props()(implicit materializer: Materializer): Props = Props(new AccountActor)

}

class AccountActor extends Actor with AppLogger with AppConf {

  override def receive: Receive = {
    case create: CreateAccount =>
      logger.info(s"create account message $create: from ${create.execer}")

      createAccount(Account(create.address, create.name, create.pubkey))
      sender ! StatusReply(201, "Account created")

      context.stop(self)
    case get: GetAccount =>
      logger.info(s"get account message $get: from ${get.execer}")
      getAccount(get.address) match {
        case Some(account) =>
          sender ! AccountReply(account.address, account.name, account.pubkey)
        case None =>
          sender ! StatusReply(404, "Account not found")
      }

      context.stop(self)
  }

}
