package com.kaputa.aplos.config

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import slick.jdbc.PostgresProfile.api._

import scala.util.Try

trait CockroachConf {
  val config = ConfigFactory.load("cockroach.conf")

  // db config
  lazy val dbName = Try(config.getString("postgres.dbName")).getOrElse("kaputa")
  lazy val dbHost = Try(config.getString("postgres.host")).getOrElse("dev.localhost")
  lazy val dbPort = Try(config.getInt("postgres.port")).getOrElse(26257)
  lazy val dbUser = Try(config.getString("postgres.user")).getOrElse("root")
  lazy val dbPass = Try(config.getString("postgres.password")).getOrElse("root")
  lazy val url = s"jdbc:postgresql://$dbHost:$dbPort/$dbName"
  lazy val driver = "org.postgresql.Driver"

  val db = {
    val config = new HikariConfig
    config.setDriverClassName(driver)
    config.setUsername(dbUser)
    config.setPassword(dbPass)
    config.setJdbcUrl(url)
    config.setConnectionTestQuery("SELECT 1")

    val ds = new HikariDataSource(config)
    Database.forDataSource(ds, Option(20))
  }
}
