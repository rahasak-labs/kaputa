package com.kaputa.aplos

import akka.actor.ActorSystem
import com.kaputa.aplos.actor.HttpActor
import com.kaputa.aplos.actor.HttpActor.Serve
import com.kaputa.aplos.cockroach.CockroachRepo
import com.kaputa.aplos.config.AppConf
import com.kaputa.aplos.util.{CryptoFactory, LoggerFactory}

object Main extends App with AppConf {

  // setup logging
  LoggerFactory.init()

  // actor system
  implicit val system = ActorSystem.create("kaputa")

  // set up keys
  CryptoFactory.init()

  // init Cockroach Repo
  CockroachRepo.init()
  system.actorOf(HttpActor.props(), name = "HttpActor") ! Serve

}
