name := "kaputa-aplos"

version := "1.0"

scalaVersion := "2.13.7"

enablePlugins(
  JavaAppPackaging,
  DockerPlugin
)

Compile / mainClass := Some("com.kaputa.aplos.Main")
Docker / packageName := "erangaeb/kaputa-aplos"
dockerExposedPorts ++= Seq(8761, 7654)
dockerEnvVars ++= Map(("COCKROACH_HOST", "dev.localhost"), ("COCKROACH_PORT", "26257"))
dockerExposedVolumes := Seq("/opt/docker/.logs", "/opt/docker/.keys")

libraryDependencies ++= {
  val akkaVersion       = "2.6.10"
  val akkaHttpVersion   = "10.1.15"

  Seq(
    "com.typesafe.akka"               %% "akka-actor"                     % akkaVersion,
    "com.typesafe.akka"               %% "akka-stream"                    % akkaVersion,
    "com.typesafe.akka"               %% "akka-slf4j"                     % akkaVersion,
    "io.spray"                        %% "spray-json"                     % "1.3.5",
    "com.typesafe.akka"               %% "akka-http"                      % akkaHttpVersion,
    "com.typesafe.akka"               %% "akka-http-spray-json"           % akkaHttpVersion,
    "com.typesafe.slick"              %% "slick"                          % "3.3.2",
    "org.postgresql"                  % "postgresql"                      % "42.2.14",
    "com.github.tminglei"             %% "slick-pg"                       % "0.18.0",
    "com.typesafe.slick"              %% "slick-hikaricp"                 % "3.3.2",
    "org.bouncycastle"                % "bcprov-jdk15on"                  % "1.69",
    "org.web3j"                       % "crypto"                          % "5.0.0",
    "org.slf4j"                       % "slf4j-api"                       % "1.7.5",
    "ch.qos.logback"                  % "logback-classic"                 % "1.0.9",
    "org.scalatest"                   % "scalatest_2.11"                  % "2.2.1"               % "test",
  )

}

resolvers ++= Seq(
  "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/",
  "Besu repository" at	"https://hyperledger.jfrog.io/artifactory/besu-maven/"
)
